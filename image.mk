
build:
	docker build --platform=linux/amd64 --build-arg app=app -f Dockerfile -t $(SERVICE):$(VERSION) .

push:
	docker tag $(SERVICE):$(VERSION) registry.gitlab.com/my_phd1/metrics-collector/$(SERVICE):$(VERSION)
	docker push registry.gitlab.com/my_phd1/metrics-collector/$(SERVICE):$(VERSION)

build-push:	build	push
