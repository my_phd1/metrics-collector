import datetime
import json

from sanic import Sanic
from sanic import json as sanic_json
from models.taskModels import DaskTask
from config.assets import set_asset

app = Sanic('metrics-collector')


@app.get('/health')
async def health(request):
    try:
        return sanic_json({'message': 'this is the dask metrics collector running'})
    except Exception as e:
        return sanic_json({'message': 'Something went wrong', 'more': e.__str__()})


@app.post('/metrics')
async def metrics(request):
    try:
        payload = json.loads(request.body)
        # print(payload)
        _key = payload.get('key')
        t = DaskTask.objects(key=_key).first()
        _start = payload.get('start')
        _finish = payload.get('finish')
        print(f'key: {_key}, start: {_start}, finish: {_finish}')
        if t:
            _started = t.started
            if (_start == 'waiting') and (_finish == 'processing'):  # the task is waiting
                pass
            elif (_start == 'processing') and (_finish == 'released'):  # the task has started
                # elif (_start == 'released') and ((_finish == 'waiting') or (_finish == 'no-worker')):  # the task has started
                t.update(set__start=_start, set__finish=_finish, set__waiting_time=datetime.datetime.now().timestamp() - _started.timestamp())
            elif (_start == 'released') and (_finish == 'forgotten'):  # the task has finished
                _finished = datetime.datetime.now()
                _waiting_time = t.waiting_time
                _duration = _finished.timestamp() - _started.timestamp()
                if _waiting_time:
                    _computation_time = _duration - _waiting_time
                    t.update(set__start=_start, set__finish=_finish, set__finished=_finished, set__duration=_duration, set__computation_time=_computation_time)
                else:
                    t.update(set__start=_start, set__finish=_finish, set__finished=_finished, set__duration=_duration)

        else:
            _task = DaskTask(started=datetime.datetime.now(), **payload).save()
        return sanic_json({'message': 'this is the dask metrics collector running'})
    except Exception as e:
        return sanic_json({'message': 'Something went wrong', 'more': e.__str__()})


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app.run('0.0.0.0', port=9989)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
