import argparse

settings = {
    'DEV': {
        'host': 'localhost',
        'port': 27017,
        'db': 'daskdb',
        'username': 'yannis',
        'password': 'yannis',

    },
    'PROD': {
        'host': 'dask-db-service.default',
        'port': 27017,
        'db': 'dask',
        'username': 'dask',
        'password': 'dask',
    },
    'TEST': {}
}


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', dest='mode', help='Set the running mode (dev/prod)')
    options = parser.parse_args()

    # Check for errors i.e if the user does not specify the target IP Address
    # Quit the program if the argument is missing
    # While quitting also display an error message
    # if not options.mode:
    # Code to handle if interface is not specified
    # parser.error("[-] Please specify the running mode (dev/prod), use --help for more info.")
    # logging.error("[-] Please specify the running mode (dev/prod), use --help for more info.")
    # else:
    #     pass
    return options


def get_mode():
    options = get_args()
    if options and options.mode:
        MODE = ('DEV' if options.mode.upper() == 'DEV' else 'PROD')
    else:
        MODE = 'DEV'
    return MODE


def get_config():
    return settings[get_mode()]
