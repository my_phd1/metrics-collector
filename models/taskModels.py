import datetime

from mongoengine import connect, DictField, DynamicDocument, StringField, DateTimeField, FloatField

from config.config import get_config

print('got in the db models file...')
conf = get_config()
try:
    db = connect(db=conf.get('db'), host=conf.get('host') + ':' + str(conf.get('port')), username=conf.get('username'),
                 password=conf.get('password'))
    print(db)
except Exception as e:
    print(e.__str__())


class DaskTask(DynamicDocument):
    key = StringField(default=None, null=True)
    start = StringField(default=None, null=True)
    finish = StringField(default=None, null=True)
    kwargs = DictField(default=None, null=True)
    started = DateTimeField(default=None, null=True)
    finished = DateTimeField(default=None, null=True)
    waiting_time = FloatField(default=None, null=True)
    computation_time = FloatField(default=None, null=True)
    duration = FloatField(default=None, null=True)


